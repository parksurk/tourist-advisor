D:\workspace\tourist-advisor>cf env tourist-advisor
Getting env variables for app tourist-advisor in org parksurk@gmail.com / space
parksurk as parksurk@gmail.com...
OK

System-Provided:
{
 "VCAP_SERVICES": {
  "dialog": [
   {
    "credentials": {
     "password": "U2Ob8TmfLEb0",
     "url": "https://gateway.watsonplatform.net/dialog/api",
     "username": "ee5d42a2-d4e6-4344-9e74-870fd19b9194"
    },
    "label": "dialog",
    "name": "dialog-service",
    "plan": "standard",
    "tags": [
     "watson",
     "ibm_created",
     "ibm_dedicated_public"
    ]
   }
  ],
  "natural_language_classifier": [
   {
    "credentials": {
     "password": "tHAujaBj4qJL",
     "url": "https://gateway.watsonplatform.net/natural-language-classifier/api"
,
     "username": "3cb5df9b-9417-424c-bb2d-b04eb96dfc42"
    },
    "label": "natural_language_classifier",
    "name": "classifier-service",
    "plan": "standard",
    "tags": [
     "watson",
     "ibm_created",
     "ibm_dedicated_public"
    ]
   }
  ]
 }
}

{
 "VCAP_APPLICATION": {
  "application_id": "a765ec68-10ff-4816-a996-e18e4e6fd433",
  "application_name": "tourist-advisor",
  "application_uris": [
   "tourist-advisor.mybluemix.net"
  ],
  "application_version": "ced0f377-e60f-4526-adbc-cac7c3c5d448",
  "limits": {
   "disk": 1024,
   "fds": 16384,
   "mem": 512
  },
  "name": "tourist-advisor",
  "space_id": "6035681a-2b61-423b-9bdf-2fb1bd1a443c",
  "space_name": "parksurk",
  "uris": [
   "tourist-advisor.mybluemix.net"
  ],
  "users": null,
  "version": "ced0f377-e60f-4526-adbc-cac7c3c5d448"
 }
}

User-Provided:
NODE_ENV: production
NPM_CONFIG_PRODUCTION: false
SECURE_EXPRESS: 1

Running Environment Variable Groups:
BLUEMIX_REGION: ibm:yp:us-south

Staging Environment Variable Groups:
BLUEMIX_REGION: ibm:yp:us-south